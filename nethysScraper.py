from bs4 import BeautifulSoup
import requests
import re
import json
from pprint import pprint

r = requests.get("http://www.archivesofnethys.com/EquipmentWeapons.aspx?Proficiency=Simple")

rootSoup = BeautifulSoup(r.text, "html5lib")

for table in rootSoup.find_all('tbody'):
    for link in table.find_all('a'):
        print("\n\nhttp://archivesofnethys.com/" + link.get('href'))

        r = requests.get("http://archivesofnethys.com/" + link.get('href'))

        regex = re.compile('(<span id="ctl00_MainContent_DataListTypes_ctl00_LabelName">)(.+?)(<\/span>)', re.DOTALL)
        dataBlock = regex.search(r.text)

        soup = BeautifulSoup(dataBlock.group(0), "html5lib")
        # print(soup.prettify())

        weapon = {}
        weapon['name'] = soup.h1.text.strip()
        for attr in soup.find_all('b'):
            if attr.text == "Source":
                sources = []
                for sibling in attr.next_siblings:
                    if sibling.name == 'a':
                        index = sibling.text.index("pg.")
                        sources.append(sibling.text[:index].strip())
                # print("Source: %s" % sources)
                weapon[attr.text.lower()] = sources[0]
            elif attr.text == "Critical":
                value = attr.next_sibling.strip(' ;')
                splitCrit = value.split('/')
                if len(splitCrit) > 1:
                    weapon['critRange'] = splitCrit[0].split('-')[0]
                    weapon['critMulti'] = splitCrit[1].replace('x', '')
                else:
                    weapon['critRange'] = '20'
                    weapon['critMulti'] = splitCrit[0].replace('x', '')
            else:
                value = attr.next_sibling.strip(' ;')
                if attr.text == "Cost":
                    attr.string.replace_with("cost")
                    value = value.split(' ')[0]
                if attr.text == "Weight":
                    attr.string.replace_with("weight")
                    value = value.split(' ')[0]
                if attr.text == "Damage":
                    attr.string.replace_with("damage")
                    value = value.split(' ')[2]
                if attr.text == "Special":
                    attr.string.replace_with("properties")
                    if value == "—":
                        value = None
                    else:
                        value = value.split(',')
                        for index, val in enumerate(value):
                            value[index] = val.strip()
                if attr.text == "Range":
                    attr.string.replace_with("rangeIncrement")
                    value = value.split(' ')[0]
                    if value == "—":
                        value = None
                if attr.text == "Type":
                    attr.string.replace_with("dmgType")
                if attr.text == "Category":
                    attr.string.replace_with(attr.string.lower())
                if attr.text == "Proficiency":
                    attr.string.replace_with(attr.string.lower())
                # print ("%s: %s" % (attr.text, repr(value)))
                weapon[attr.text] = value
        weapon['description'] = soup.find_all('h3')[1].next_sibling
        pprint (weapon)
        r = requests.post("http://localhost:5000/api/weapons/create", json=weapon)
        print (r)
