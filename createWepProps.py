import requests
import json

data = [
    {
        "name": "Blocking",
      	"description": "Weapons and armor with the fragile quality cannot take \
        the beating that sturdier weapons can. A fragile weapon gains the \
        broken condition if the wielder rolls a natural 1 on an attack roll \
        with the weapon. If a fragile weapon is already broken, the roll of a \
        natural 1 destroys it instead. Masterwork and magical fragile weapons \
        and armor lack these flaws unless otherwise noted in the item \
        description or the special material description.",
    	"source": "Ultimate Combat"
    },
    {
        "name": "Brace",
      	"description": "If you use a readied action to set a brace weapon \
        against a charge, you deal double damage on a successful hit against \
        a charging creature.",
    	"source": "Core"
    },
    {
        "name": "Deadly",
      	"description": "When you use this weapon to deliver a coup de grace, \
        it gains a +4 bonus to damage when calculating the DC of the Fortitude \
        saving throw to see whether the target of the coup de grace dies from \
        the attack. The bonus is not added to the actual damage of the coup de \
        grace attack.",
    	"source": "Ultimate Combat"
    },
    {
        "name": "Disarm",
      	"description": "When you use a disarm weapon, you get a +2 bonus on \
        Combat Maneuver Checks to disarm an enemy.",
    	"source": "Core"
    },
    {
        "name": "Distracting",
      	"description": "You gain a +2 bonus on Bluff skill checks to feint in \
        combat while wielding this weapon.",
    	"source": "Ultimate Combat"
    },
    {
        "name": "Double",
      	"description": "You can use a double weapon to fight as if fighting \
        with two weapons, but if you do, you incur all the normal attack \
        penalties associated with fighting with two weapons, just as if you \
        were using a one-handed weapon and a light weapon. You can choose to \
        wield one end of a double weapon two-handed, but it cannot be used as \
        a double weapon when wielded in this way—only one end of the weapon \
        can be used in any given round.",
    	"source": "Core"
    },
    {
        "name": "Fragile",
      	"description": "Weapons and armor with the fragile quality cannot take \
        the beating that sturdier weapons can. A fragile weapon gains the \
        broken condition if the wielder rolls a natural 1 on an attack roll \
        with the weapon. If a fragile weapon is already broken, the roll of a \
        natural 1 destroys it instead. Masterwork and magical fragile weapons \
        and armor lack these flaws unless otherwise noted in the item \
        description or the special material description.\n\nIf a weapon gains \
        the broken condition in this way, that weapon is considered to have \
        taken damage equal to half its hit points +1. This damage is repaired \
        either by something that addresses the effect that granted the weapon \
        the broken condition (like quick clear in the case of firearm misfires \
        or the Field Repair feat) or by the repair methods described in the \
        broken condition. When an effect that grants the broken condition is \
        removed, the weapon regains the hit points it lost when the broken \
        condition was applied. Damage done by an attack against a weapon (such \
        as from a sunder combat maneuver) cannot be repaired by an effect that \
        removes the broken condition.",
    	"source": "Ultimate Combat"
    },
    {
        "name": "Grapple",
      	"description": "On a successful critical hit with a weapon of this \
        type, you can grapple the target of the attack. The wielder can then \
        attempt a combat maneuver check to grapple his opponent as a free \
        action. This grapple attempt does not provoke an attack of opportunity \
        from the creature you are attempting to grapple if that creature is \
        not threatening you. While you grapple the target with a grappling \
        weapon, you can only move or damage the creature on your turn. You are \
        still considered grappled, though you do not have to be adjacent to \
        the creature to continue the grapple. If you move far enough away to \
        be out of the weapon’s reach, you end the grapple with that action.",
    	"source": "Ultimate Combat"
    },
    {
        "name": "Monk",
      	"description": "A monk weapon can be used by a monk to perform a \
        flurry of blows.",
    	"source": "Core"
    },
    {
        "name": "Nonlethal",
      	"description": "These weapons deal nonlethal damage.",
    	"source": "Core"
    },
    {
        "name": "Performance",
      	"description": "When wielding this weapon, if an attack or combat \
        maneuver made with this weapon prompts a combat performance check, \
        you gain a +2 bonus on that check.",
    	"source": "Core"
    },
    {
        "name": "Reach",
      	"description": "You use a reach weapon to strike opponents 10 feet \
        away, but you can’t use it against an adjacent foe.",
    	"source": "Core"
    },
    {
        "name": "Sunder",
      	"description": "When you use a sunder weapon, you get a +2 bonus on \
        Combat Maneuver Checks to sunder attempts.",
    	"source": "Core"
    },
    {
        "name": "Trip",
      	"description": "You can use a trip weapon to make trip attacks. If you \
        are tripped during your own trip attempt, you can drop the weapon to \
        avoid being tripped.",
    	"source": "Core"
    },
    {
        "name": "See Text",
      	"description": "",
    	"source": ""
    }
]

for d in data:
    print (json.dumps(d))
    r = requests.post("http://localhost:5000/api/weapons/property/create", json=d)
